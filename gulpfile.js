const del = require('del');
const image = require('gulp-image');
const sass = require('gulp-sass');
const browserSync = require('browser-sync');
const gulp = require('gulp');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const uglify = require('gulp-uglify');
const sourcemaps = require('gulp-sourcemaps');
const rigger = require('gulp-rigger');
const cssmin = require('gulp-minify-css');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const reload = browserSync.reload;


gulp.task('clean', function() {
    return del.sync('build'); // Delete folder before building
});

gulp.task('fonts', function() {
    gulp.src('./src/assets/fonts/')
        .pipe(gulp.dest('./build/assets/fonts'))
});

gulp.task('image', function() {
    gulp.src('./src/assets/img/*')
        .pipe(image())
        .pipe(imagemin({ //Сожмет img
            progressive: true,
            svgoPlugins: [{ removeViewBox: false }],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest('./build/assets/img'))
        .pipe(reload({ stream: true }));

});

gulp.task('html', function() {
    gulp.src('./src/**/*.html')
        .pipe(rigger())
        .pipe(gulp.dest('./build'))
        .pipe(reload({ stream: true }));


});

gulp.task('sass', function() {
    return gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.init())
        .pipe(concat('style.css'))
        .pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(autoprefixer()) //Добавит префиксы
        .pipe(gulp.dest('./build/styles'))
        .pipe(reload({ stream: true }));

});

gulp.task('js', function() {
    gulp.src('./src/**/*.js')
        .pipe(rigger())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./build/'))
        .pipe(reload({ stream: true }));

});

gulp.task('browserSync', function() {
    browserSync({
        server: {
            baseDir: './build/'
        }
    })
});

gulp.task('watch', ['sass', 'html', 'js', 'image', 'browserSync'], function() {
    gulp.watch('./src/styles**/*.scss', ['sass']);
    gulp.watch('./src/assets/img/*', ['image']);
    gulp.watch('./src/**/*.html', ['html']);
    gulp.watch('./src/js**/*.js', ['js']);
    gulp.watch('build/*.html', browserSync.reload);
    gulp.watch("./build/**/*.css").on("change", browserSync.reload);
    gulp.watch('./build/**/*.js').on("change", browserSync.reload);
});

gulp.task('default', ['watch', 'image']);
